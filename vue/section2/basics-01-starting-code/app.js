const app = Vue.createApp({
    data() {
        return {
			courseGoalA: '<h3>Learn Vue</h3>'
			,courseGoalB: 'Master Vue'
			,vueLink: 'http://vuejs.org/'
			,counter: 0
			,firstName: ''
			,lastName: ''
			,fullName: ''
			,currentGoal: ''
			,changedGoal: ''
        };
	}
	,computed: {
		fullName() {
			return this.firstName + ' ' + this.lastName;
		}		
	}
	,watch: {
		firstName(newvalue) {
			this.fullName = newvalue + ' ' + this.lastName;
		}
		,lastName(newvalue) {
			this.fullName = this.firstName + ' ' + newvalue;
		}
		,counter(newvalue) {
			if (newvalue > 50) {
				this.counter = 0;
			}
		}
		,currentGoal(newvalue, oldvalue) {
			if (newvalue !== oldvalue) {
				this.changedGoal = 'changed!!';
			} else {
				this.changedGoal = 'no change';
			}
		}
	}
	,methods: {
		increaseCounter(nAmount) {
			this.counter += nAmount;
		}
		,reduceCounter(nAmount) {
			this.counter -= nAmount;
		}
		,resetCounter() {
			this.counter = 0;
		}
		,resetName() {
			this.firstName = '';
			this.lastName = '';
		}
		,outputGoal() {
			const randomNumber = Math.random();
			if (randomNumber < 0.5) {
				this.currentGoal = this.courseGoalA
				return this.courseGoalA;
			} else {
				this.currentGoal = this.courseGoalB;
				return this.courseGoalB;
			}
		}
	}
});
app.mount('#user-goal');