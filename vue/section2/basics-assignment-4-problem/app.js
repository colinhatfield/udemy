const app = Vue.createApp({
	data() {
		return {
			userInput: ''
			,displayHidden: 'visible'
			,backgroundColor: ''
		}
	}
	,methods: {
		clickToggle() {
			if (this.displayHidden != 'visible') {
				this.displayHidden = 'visible'
			} else {
				this.displayHidden = 'hidden';
			}
		}
	}
});

app.mount('#assignment');