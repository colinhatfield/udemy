const app = Vue.createApp({
	data() {
		return {
			displayList: true
			,tasks: []
			,enteredTask: ''
		}
	}
	,computed: {
		displayListButtonText() {
			if (this.displayList) {
				return "hide list";
			} else {
				return "show list";
			}
		}
	}
	,methods: {
		addTask() {
			this.tasks.push(this.enteredTask);
		}
		,toggleDisplayList() {
			this.displayList = !this.displayList;
		}
	}
});
app.mount('#assignment')