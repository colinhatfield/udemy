function getRandomValue(minValue, maxValue) {
	return Math.floor(Math.random() * (maxValue - minValue)) + minValue;
}

const app = Vue.createApp({
	data() {
		return {
			playerHealth: 100
			,monsterHealth: 100
			,currentRound: 0
			,result: null
			,logMessages: []
		}
	}
	,computed: {
		monsterBarStyles() {
			return {width: this.monsterHealth + '%'};
		}
		,playerBarStyles() {
			return {width: this.playerHealth + '%'}
		}
		,mayUseSpecialAttack() {
			return this.currentRound % 3 !==0;
		}
	}
	,watch: {
		playerHealth(value) {
			if (value <= 0 && this.monsterHealth <= 0) {
				this.result = 'draw';
			} else if (value <= 0) {
				this.result = 'monster wins';
			}
		}
		,monsterHealth(value) {
			if (value <= 0 & this.playerHealth <= 0) {
				this.result = 'draw';
			} else if (value <= 0 ) {
				this.result = 'player wins';
			}
		}
	}
	,methods: {
		attackMonster() {
			const attackValue = getRandomValue(5, 12);
			this.monsterHealth -= attackValue;
			if (this.monsterHealth < 0) {
				this.monsterHealth = 0;
			}
			this.addLogMessage('player','attack', attackValue);
			this.attackPlayer();
		}
		,attackPlayer() {
			this.currentRound += 1;
			const attackValue = getRandomValue(8, 15);
			this.playerHealth -= attackValue;
			if (this.playerHealth < 0) {
				this.playerHealth = 0;
			}
			this.addLogMessage('monster','attack', attackValue);
		}
		,specialAttackMonster() {
			const attackValue = getRandomValue(10, 25);
			this.monsterHealth -= attackValue;
			if (this.monsterHealth < 0) {
				this.monsterHealth = 0;
			}
			this.addLogMessage('player','attack', attackValue);
			this.attackPlayer();	
		}
		,healPlayer() {
			const healValue = getRandomValue(8, 20);
			this.playerHealth += healValue;
			if (this.playerHealth > 100) {
				this.playerHealth = 100;
			}
			this.addLogMessage('player','heal', healValue);
			this.attackPlayer();
		}
		,surrender() {
			this.addLogMessage('player','surrenders', 0);
			this.result = 'monster wins';
		}
		,resetGame() {
			this.playerHealth = 100;
			this.monsterHealth = 100;
			this.currentRound = 0;
			this.result = null;
			this.logMessages = [];
		}
		,addLogMessage(who, what, value) {
			this.logMessages.unshift({
				actionBy: who
				,actionType: what
				,actionValue: value
			});
		}
	}
});
app.mount('#game');