const app = Vue.createApp({
  data() {
    return {
      currentUserInput: '',
      message: 'Vue is great!',
    };
  },
  methods: {
    saveInput(event) {
      this.currentUserInput = event.target.value;
    },
    setText() {
		//this.message = this.currentUserInput;
		this.message = this.$refs.userText.value;
		console.log(this.$refs.userText.value);
    },
  	}
  	,beforeCreate() {
	  	console.log('beforeCreate fired');
  	}
 	,created() {
	  	console.log('created fired');
  	}
  	,beforeMount() {
		console.log('beforeMount fired');
	}
	,mounted() {
		console.log('mounted fired');
	}
	,beforeUpdate() {
		console.log('beforeUpdate fired');
	}
	,updated() {
		console.log('updated fired');
	}
	,beforeUnmount() {
		console.log('beforeUnmount fired');
	}
	,unmounted() {
		console.log('unmounted fired');
	}
});
app.mount('#app');

setTimeout(function() {
	app.unmount();
}, 10000)

/********************************* */

// showing how java is NOT reactive

let message = 'hello';
let longMessage = message + 'world';
console.log(longMessage);
message = 'good bye';
console.log(longMessage);

//proxies --> How Vue works under the hood.

/*
const data = {
	message: 'hello'
	,longMessage: 'hello world'
};
const handler = {
	set(target, key, value) {
		console.log(target);
		console.log(key);
		console.log(value);

		if (key === 'message') {
			target.longMessage = value + ' world';
		}

	}
}
const proxy = new Proxy(data, handler);
proxy.message = 'good bye';
console.log(proxy.longMessage);
*/

/****************************************** */

// you can have multiple vue apps on a page
const app2 = Vue.createApp({
	data() {
	  return {
		favouriteMeal: 'pizza'
	  };
	}
	,template: 
		`<p>{{ favouriteMeal }}</p>`
  });
  app2.mount('#app2');