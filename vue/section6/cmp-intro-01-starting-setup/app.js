const app = Vue.createApp({
	data() {
		return {
			friends: [
				{
					id: 234656
					,name: 'Colin Hatfield'
					,phone: '416 729-8330'
					,email: 'colin.hatfield@gmail.com'
				}
				,{
					id: 234556
					,name: 'Laura Novog'
					,phone: '416 459-7556'
					,email: 'laura.novog@gmail.com'
				}
			]
		}
	}
});

app.component('friend-contact', {
	data() {
		return { 
			detailsAreVisible: false
			,friend: 
			{
				id: 234656
				,name: 'Colin Hatfield'
				,phone: '416 729-8330'
				,email: 'colin.hatfield@gmail.com'
			}
		}
	}
	,methods: {
		toggleDetails() {
			this.detailsAreVisible = !this.detailsAreVisible;
		}
	}
	,template: 
		`<li>
			<h2>{{ friend.name }}</h2>
			<button @click="toggleDetails()">
				{{ detailsAreVisible? 'Hide':'Show' }} Details
	 		</button>
			<ul v-if="detailsAreVisible">
	  			<li><strong>Phone:</strong>{{ friend.phone }}</li>
	  			<li><strong>Email:</strong>{{ friend.email }}</li>
			</ul>
  		</li>`
});

app.mount('#app');